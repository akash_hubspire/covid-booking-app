import React from 'react'
import { View, Text } from 'react-native'
import CommonStyle from '../commonStyle/CommonStyle';
import Button from '../components/Button';
import CentersThisWeek from './CentersThisWeek';

const CentersToday = ({route}) => {
    const districtid = route?.params?.requestedDistrict

    
    return (
        <>
        <CentersThisWeek distNo={districtid}/>
        </>
    )
}

export default CentersToday
