import React, {useEffect, useState} from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Platform, Alert } from 'react-native'
import CommonStyle from '../commonStyle/CommonStyle';
import Button from '../components/Button';
import FilterContainer from '../components/FilterContainer'
import VacinationCentersList from '../components/VacinationCentersList'
import axios from 'axios';

const CentersThisWeek = ({distNo,route}) => {

    console.log("DistNo", distNo)
    

    const [ loading, setLoading ] = useState(false)

    const [centers, setCenters] = useState([])
  
    const districtid = distNo ? distNo : route?.params?.requestedDistrict
    const pinCode = route?.params?.requestedPincode
    // console.log(pinCode, centers)
    const today = new Date();

    const date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear()
    // console.log(date)
    // pin 781346
    useEffect(() => {
        (async () => {
            setLoading(true)
            // await axios.get(`https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByDistrict?district_id=${districtid}&date=${date}`)
            await axios.get(pinCode ? `https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/findByPin?pincode=${pinCode}&date=${date}`
            : `https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByDistrict?district_id=${districtid}&date=${date}`)
            .then((response) => {
                // console.log(response.data)
                let centerData = pinCode ? response?.data?.sessions : response?.data?.centers
                
                // let session = JSON.stringify(centerData?.sessions)
                // session.keys(session).forEach((item)=>console.log(session[item]))
                // console.log('response',JSON.stringify(response.data.centers))
                setCenters(centerData)
                setLoading(false);
                
            })
            .catch((error) => {
                // Alert.alert(error?.message)
                console.log(error?.message);
                setLoading(false);

            })
            // return ()=>{}
        })()
        
    }, [])
    
    return (
        <View style={[CommonStyle.mainContainer,{ flex: 1,justifyContent:'space-between', backgroundColor:'#E5E5E5', paddingHorizontal:15}]}>
        {
            centers.length !==0 ?
            <>
                <View style={{marginBottom:90}}>
                    <FilterContainer/>
                    
                    <VacinationCentersList centerList={centers}/>
                </View>
                <Button title={"Notify"} buttonStyle={{position:'absolute', width:'100%', bottom:Platform.OS === 'ios' ? 60 : 50}} onPressAction={() => {}}/>
            </>
            
                :
                <View  style={{flex:1,alignItems:'center', justifyContent:'center'}}>
                    <Text>Nothing to show here</Text>
                </View>
            }

        </View>
    )
}


export default CentersThisWeek
