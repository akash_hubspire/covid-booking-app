import React, {useState, useEffect} from 'react'
import { 
    View, 
    ScrollView,
    Text, 
    StyleSheet,
    TextInput,
    Image,
    TouchableOpacity,
    ActivityIndicator,
    Platform,
    KeyboardAvoidingView,
    Dimensions,
    Alert,
} from 'react-native'
import FormInput from '../components/FormInput';
import Button from '../components/Button';
import { NavigationContainer } from '@react-navigation/native';
import CommonStyle from '../commonStyle/CommonStyle';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Login = ({navigation}) => {
    const [ loading, setLoading ] = useState(false)
    const [ mobile, setMobile ] = useState('')
    const [ password, setPassword ] = useState('')
    // console.log(mobile,password)
    
    useEffect(() => {
        // setLoading(true)
        (async () => {
            const auth = await AsyncStorage.getItem("LoggedIn")
            if (auth && auth === '1') {
                // null
                Alert.alert("Authenticated","Redirecting to Home")
                navigation.navigate("Home")
            }
            // return ()=>{}
        })()
        
    }, [])


    const isMobileValidated = () => {
        if (mobile.length === 10){
             return true
        } else {
             Alert.alert("Invalid Phone Number", "Mobile number must contain 10 numbers")
        }
    }

    const isPasswordValidated = () => {
        if (password.length >= 8){
             return true
        } else {
             Alert.alert("Invalid Password", "Password must contain atleast 8 characters")
        }
    }

    const isValidated = () => {
        if (isMobileValidated() && isPasswordValidated()){
            return true
        } else{
            return false
        }
    }


    const loginHandle = async () => {
        let loginCred = {"phone": mobile, "password": password}
        if (isValidated()) {
            setLoading(true)
            try {
                await AsyncStorage.setItem("LoggedIn",'1')
                setTimeout(() => {
                    Alert.alert("Login Success",JSON.stringify(loginCred))
                    navigation.navigate("Home")
                    setLoading(false)
                }, 2000);

            } catch (error) {
                Alert.alert(error)
                setLoading(false)
            }
        } else{
            return
            // Alert.alert("Error","Wrong Credentials")
        }
    }

    return (
        <KeyboardAvoidingView 
            style={{flex:1, backgroundColor:'white'}}
            behavior={Platform.OS === "ios" ? 'padding' : null }
        >
            
                <View style={[CommonStyle.loginContainer,{justifyContent:'space-between', 
                    paddingBottom:Platform.OS === 'ios' ? 60 : 0}]}
                >
                    
                    <Text style={styles.title}>Welcome</Text>
                    <Text style={styles.subtitle}>vaccinator & verifier</Text>

                    <FormInput title={"Mobile Number"} formStyle={{marginTop: Platform.OS === 'ios' ? 0 : -5}} onChangeTextAction={(value) => setMobile(value)} placeholder={"Mobile Number"} keyboard={"numeric"}/>
                    <FormInput title={"Password"} formStyle={{marginTop: Platform.OS === 'ios' ? 0 : -15}} onChangeTextAction={(value) => setPassword(value)} placeholder={"Password"} secureInput={true}/>
                    
                    <Button title={"Login"} buttonStyle={{marginTop:Platform.OS === 'ios' ? 35 : 30}} isLoading={loading} onPressAction={loginHandle} />
                </View>
                <Image source={require('../assets/Groupddd.png')} style={styles.image}/>
            
        </KeyboardAvoidingView>
    )
}


const styles = StyleSheet.create({
   title: {
       fontSize: 30,
       marginTop: Platform.OS === 'ios' ? 90 : 30,
       color:'grey',
   },
   subtitle: {
       color:'grey',
       marginTop: Platform.OS === 'ios' ? 0 : -5
   },
    image:{
        width: Dimensions.get('window').width,
        position:'relative',
        top: Platform.OS === 'ios' ? 0 : 40,
    }
  });


export default Login
