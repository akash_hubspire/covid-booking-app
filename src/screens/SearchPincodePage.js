import React,{ useState } from 'react'
import { View, Text } from 'react-native'
import FormInput from '../components/FormInput'
import CommonStyle from '../commonStyle/CommonStyle';
import Button from '../components/Button';

const SearchPincodePage = ({navigation}) => {
    const [ loading, setLoading ] = useState(false)
    
    const [ pincode, setPincode ] = useState('')
    // console.log(props)

    return (
        <View style={[CommonStyle.mainContainer,{justifyContent:'space-between'}]}>
            <FormInput title={"Pincode"} 
                placeholder={"Enter pincode"}
                keyboard={"numeric"}
                onChangeTextAction={(val) => setPincode(val)}
            />
            <Button title={"Search"} 
                isLoading={loading}
                buttonStyle={{ position:'relative' }} 
                onPressAction={() => {navigation.navigate('Result',
                    {
                        screen: 'This Week',
                        params: { requestedPincode: pincode },
                    }
                )}}
            />
        </View>
    )
}

export default SearchPincodePage
