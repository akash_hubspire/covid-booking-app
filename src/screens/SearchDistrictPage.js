import React, {useEffect, useState} from 'react'
import { View, Text,Modal, StyleSheet, TouchableOpacity,Pressable, Image, Dimensions, ScrollView, Alert } from 'react-native'
import FormInput from '../components/FormInput'
import CommonStyle from '../commonStyle/CommonStyle';
import Button from '../components/Button';
import axios from 'axios';
import {Picker} from '@react-native-picker/picker';


const SearchPincodePage = ({navigation}) => {
    const [ loading, setLoading ] = useState(false)
    const [ stateId, setStateId ] = useState('')
    const [ districtId, setDistrictId ] = useState('')
    const [ districtSelected, setDistrictSelected ] = useState(false)
    const [ stateName, setStateName ] = useState('Select state')
    const [ stateSelected, setStateSelected ] = useState(false)
    const [ districtName, setDistrictName ] = useState('Select District')
  
    const [ modalVisible, setModalVisible ] = useState(false)
    const [ stateList, setStateList ] = useState([])
    const [ districtList, setDistrictList ] = useState([])



    useEffect(() => {
        (async () => {
            setLoading(true)
            await axios.get('https://cdndemo-api.co-vin.in/api/v2/admin/location/states')
            .then((response) => {
                // let stateList = response.data.states;
                setStateList(response.data.states)
                setLoading(false);
                // console.log(response.data.states);
            })
            .catch((error) => {
                Alert.alert(error)
                console.log(error);
                setLoading(false);

            })
            // return ()=>{}
        })()
        
    }, [])

    const districtHandle = async () => {
        setLoading(true)
        await axios.get(`https://cdn-api.co-vin.in/api/v2/admin/location/districts/${stateId}`)
        .then((response) => {
            
            setDistrictList(response.data.districts)
            
            setLoading(false)
            setModalVisible(true)
        })
        .catch((error) => {
            Alert.alert(error)
            console.log(error);
            setLoading(false)
        })
    }

    return (
        <View style={[CommonStyle.mainContainer,{justifyContent:'space-between'}]}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                setModalVisible(!modalVisible);
                }}
            >
                <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <Pressable
                    style={styles.buttonClose}
                    onPress={() => setModalVisible(!modalVisible)}
                    >
                        <Text style={styles.textStyle}>X</Text>
                    </Pressable>
                    <ScrollView>
                    { !stateSelected ? stateList.map((state)=> (

                            <TouchableOpacity 
                                style={{borderWidth:1, marginTop: 5, borderRadius: 5,alignItems: 'center',borderColor:'grey'}}
                                key={state.state_id}
                                onPress={()=>{
                                    setStateId(state.state_id),
                                    setStateName(state.state_name),
                                    setStateSelected(true),
                                    setDistrictSelected(true)
                                    setModalVisible(!modalVisible)
                                    
                                }}
                            >
                                <Text style={styles.modalText}>{state.state_name}</Text>

                            </TouchableOpacity>
                    )) : districtList.map((district)=> (

                        <TouchableOpacity 
                            style={{borderWidth:1, marginTop: 5, borderRadius: 5,alignItems: 'center',borderColor:'grey'}}
                            key={district.district_id}
                            onPress={()=>{
                                setDistrictId(district.district_id),
                                setDistrictName(district.district_name),
                                setStateSelected(false),
                                setDistrictSelected(true),
                                setModalVisible(!modalVisible)
                            }}
                        >
                            <Text style={styles.modalText}>{district.district_name}</Text>

                        </TouchableOpacity>
                ))}
                        
                    </ScrollView>
                </View>
                </View>
            </Modal>
            <View  style={{flex:1,opacity: modalVisible ? .2 : 1}}>
                <View style={{flex:1}}> 
                    {/* <View style={styles.picker}>
                        <Text >Select State</Text>
                        <View style={styles.pickerInput}>
                            <Picker
                            style={{ height: 50}}
                                selectedValue={selectedState}
                                onValueChange={(itemValue, itemIndex) =>
                                setSelectedState(itemValue)
                            }>
                                <Picker.Item label="Java" value="java" />
                                <Picker.Item label="JavaScript" value="js" />
                            </Picker>
                        </View>
                    </View> */}


                    <View style={{marginTop:20}}>
                        <Text style={{color:'grey'}}>State</Text>
                        <TouchableOpacity onPress={()=>{setModalVisible(true)}}>
                            <View style={styles.cpicker}>
                                <Text style={{color:'grey'}}>
                                    {stateName}
                                </Text>
                                <Image source={require('../assets/downCaret.png')} style={styles.arrow}/>

                            </View>
                        </TouchableOpacity>
                    </View>

                    
                    <View style={{marginTop:20}}>
                        <Text style={{color:'grey'}}>District</Text>
                        <TouchableOpacity disabled={!stateSelected} onPress={districtHandle}>
                            <View style={[styles.cpicker, {opacity: districtSelected ? 1 : .5,}]}>
                                <Text style={{color:'grey'}}>
                                    {districtName}
                                </Text>
                                <Image source={require('../assets/downCaret.png')} style={styles.arrow}/>
                            </View> 
                        </TouchableOpacity>
                    </View>

                </View>
                <Button isLoading={loading} title={"Search"} buttonStyle={{ position:'relative' }} onPressAction={() => {navigation.navigate('Result',
                        {
                            screen: 'This Week',
                            params: {   
                                // requestedState: stateName, 
                                requestedDistrict: districtId 
                            },
                        }
                    )}}
                />

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    picker:{
        marginTop:20
    },
    pickerInput:{marginTop:5,borderColor:'black', borderWidth:1 },
    // ===========================
    cpicker:{
        
        flexDirection:'row',
        justifyContent:'space-between',
        borderWidth:1,
        borderColor:'grey',
        padding:10,
        borderRadius:5
    },
    arrow:{
        height:15,
        width:13,
        tintColor:'grey'
    },
    // =======================modal
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        // marginTop: 22/
      },
    modalView: {
        margin: 20,
        height:Dimensions.get('window').height *.8,
        width: Dimensions.get('window').width *.8,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        elevation:5
    },
    
    buttonClose: {
        marginBottom:20,
        padding:10,
        width:40,
        height:40,
        borderRadius:50,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor: "blue",
    },
        textStyle: {
        color: "white",
        fontWeight: "bold",
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center",
        paddingHorizontal:50,
        paddingTop:20
    }
})

export default SearchPincodePage
