import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Login from './screens/Login';
import SearchPincodePage from './screens/SearchPincodePage'
import SearchDistrictPage from './screens/SearchDistrictPage'
import CentersToday from './screens/CentersToday'
import CentersThisWeek from './screens/CentersThisWeek'


const Stack = createNativeStackNavigator();
const TopTab = createMaterialTopTabNavigator();

const HomeStack = () => {
    return (
        <TopTab.Navigator>
            <TopTab.Screen name='Search By Pincode' component={SearchPincodePage} />
            <TopTab.Screen name='Search By District' component={SearchDistrictPage} />
        </TopTab.Navigator>
    )
}

const ResultStack = () => {
    return (
        <TopTab.Navigator>
            <TopTab.Screen name='Today' component={CentersToday}/>
            <TopTab.Screen name='This Week' component={CentersThisWeek}/>
        </TopTab.Navigator>
    )
}

const AppNew = () => {

    return (
        <NavigationContainer >
            <Stack.Navigator initialRouteName="Home" screenOptions={{}}>
                <Stack.Screen name="Login" 
                    options={{headerShown: false}} component={Login} />
                <Stack.Screen name="Home" 
                    options={{ title: 'Home'}} component={HomeStack} />
                <Stack.Screen name="Result" 
                    options={{ title: 'Vaccine Centers'}} component={ResultStack} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}



export default AppNew
