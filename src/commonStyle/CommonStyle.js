import { StyleSheet } from 'react-native';

export default StyleSheet.create({
   mainContainer: {
      flex: 1,
      paddingHorizontal: 24,
      backgroundColor:'white',
      paddingBottom:26
    },
   loginContainer: {
      paddingHorizontal: 24,
      backgroundColor:'white',
      paddingBottom:26
    },
});
