import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Switch, Platform } from 'react-native'

const FilterBtns = ({title}) => {
    const [ filterClicked, setFilterClicked ] = useState(false)
    // console.log(filterClicked)
    return (
        <TouchableOpacity style={[styles.filterBtns, 
            {backgroundColor: filterClicked ? 'blue' : 'white',}]} 
            onPress={() => setFilterClicked(!filterClicked)}>
                <Text style={{fontSize:12, color: filterClicked 
                    ? 'white' : 'black',}}>
                        {title}
                </Text>
        </TouchableOpacity>
    )
}

const FilterContainer = ({text}) => {

    const [ isEnabled, setIsEnabled ] = useState(false)
    const toggleSwitch = () => setIsEnabled(!isEnabled)
 

    return (
        <View style={styles.filterContainer}>
            <View style={{flex:8,flexDirection:'row', justifyContent:'space-evenly'}}>
                <FilterBtns title={'18+'}/>
                <FilterBtns title={'45+'}/>
                <FilterBtns title={'Free'}/>
                <FilterBtns title={'Paid'}/>
            </View>
            <View style={{flex:5,flexDirection:'row'}}>
                <Text style={[styles.filterBtns,{backgroundColor:'transparent'}]}>First Dose</Text>
                <Switch
                    
                    trackColor={{false:'#76577', true:'white'}}
                    thumbColor={isEnabled ? '#014ED0' : 'grey'}
                    onValueChange={toggleSwitch}
                    value={isEnabled}
                />
            </View>
        </View>
    )
}
const styles= StyleSheet.create({
    filterBtns:{
        paddingHorizontal:10,
        paddingVertical:5,
        paddingTop: Platform.OS === 'ios' ? 10 : null,
        borderRadius:20,
    },
    filterContainer:{
        marginVertical:15,
        flexDirection:'row',
        justifyContent:'space-evenly',
    }
})
export default FilterContainer
