import React from 'react'
import { View, TouchableOpacity, StyleSheet, Text, ActivityIndicator } from 'react-native'

const Button = ({title, onPressAction, buttonStyle, isLoading}) => {
    // console.log(isLoading)
    return (
        <View>
            <TouchableOpacity
                style={[styles.button, buttonStyle]}
                onPress={onPressAction}
            >
            { isLoading ? <ActivityIndicator size="small" color="white"/>
            : <Text style={{color:'white'}}>{title}</Text>
            }
                
            </TouchableOpacity>
                
        </View>
    )
}

const styles= StyleSheet.create({
    button:{
        marginTop:20,
        alignItems: "center",
        backgroundColor: "#014ED0",
        borderRadius:5,
        padding: 15,
        marginBottom: Platform.OS === 'ios' ? 20 : 0
    }
})
export default Button
