import React, {useEffect, useState} from 'react'
import { View, Text, FlatList, Alert, StyleSheet } from 'react-native'
import axios from 'axios'


const renderVaccinationCenters = (center) => {
    
    // console.log('CENTER+++++',center)
    let {name, address,sessions} = center?.item
    sessions = JSON.stringify(sessions[0])
    // sessions = sessions)
    // console.log('+++SESSION+++',JSON.parse(sessions).available_capacity_dose2)
    return (
        <View style={styles.centerCard}>
            
            <Text style={styles.centerName}>{name}</Text>
            <Text style={{marginTop:Platform.OS === 'ios' ? 10 : 5}}>{address}</Text>
            <View style={styles.doses}>
                <View style={{marginRight:25}}>
                    <View style={[styles.doseChild, 
                        {backgroundColor: JSON.parse(sessions).available_capacity_dose1 < 10 ? 'red' : 'green'
                    }]}>
                        <Text style={{color:'white'}}>{JSON.parse(sessions).available_capacity_dose1}</Text>                       
                    </View>
                    <Text style={styles.doseName}>Dose 1</Text>
                </View>
                <View>
                    <View style={[styles.doseChild, 
                        {backgroundColor: JSON.parse(sessions).available_capacity_dose2 < 10 ? 'red' : 'green'
                    }]}>
                        <Text style={{color:'white'}}>{JSON.parse(sessions).available_capacity_dose2}</Text>                       
                    </View>
                    <Text style={styles.doseName}>Dose 2</Text>

                </View>

            </View>
        </View>
    )
}
const VacinationCentersList = ({centerList}) => {
    let centers = centerList
    // console.log('RES CENTER',centers)
    return (
        <>
            <FlatList 
                data={centers}
                renderItem={renderVaccinationCenters}
                keyExtractor={(center) => center.center_id}
            />
        </>
    )
}

const styles = StyleSheet.create({
    centerCard:{
        backgroundColor:'white',
        marginBottom:20,
        padding:20,
        borderRadius:5
    },
    centerName:{
        fontSize:20,
        fontWeight:'600',
        textTransform: 'capitalize',
    },
    doses:{
        flexDirection:'row',
        marginTop: Platform.OS === 'ios' ? 20: 10
    },
    doseChild:{
        padding:8,
        borderRadius: 7,
        alignItems:'center',
    },
    doseName:{
        marginTop:Platform.OS === 'ios' ? 10 : 10,
        fontSize: 13
    }
    
})

export default VacinationCentersList

