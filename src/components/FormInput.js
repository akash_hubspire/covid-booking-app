import React from 'react'
import { View, Text, StyleSheet, TextInput } from 'react-native'

const FormInput = ({ title, formStyle, placeholder,onChangeTextAction, keyboard,secureInput=false }) => {
    return (
        <View style={formStyle}>
            <Text style={{ marginTop: 22, color: 'grey' }}>{title}</Text>
            <TextInput
                style={styles.input}
                placeholder={placeholder}
                placeholderTextColor='grey'
                keyboardType={keyboard}
                secureTextEntry={secureInput}
                onChangeText={onChangeTextAction}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    input: {
        height: 40,
        borderRadius: 5,
        borderColor: 'grey',
        borderWidth: 1,
        padding: 10,
        color:'black'
        // backgroundColor: 'red',
    },
})
export default FormInput
